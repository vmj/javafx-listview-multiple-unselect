package app;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.logging.Logger;
import java.util.stream.Stream;

import static java.util.Arrays.stream;

public final class Main extends Application {
	private final Logger log = Logger.getGlobal();

	@Override
	public void start(final Stage stage) {
		final var paragraphs = Stream.of(
				"Press Control key while selecting a few items.  " +
						"You will see output on the console for each additional selected item.",
				"While pressing control and unselecting an item, " +
						"no change notification is seen until the last selected item is unselected.",
				"At any point, you can click the 'Print selection' button " +
						"to see that the selection does change, though."
		).map(Text::new).toArray(Text[]::new);

		final var instructions = new VBox(10, paragraphs);
		instructions.setPadding(new Insets(10));

		final var listView = new ListView<String>();
		listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		listView.setItems(FXCollections.observableArrayList("apple", "banana", "cucumber", "dewberry", "eggplant"));
		listView.getSelectionModel().selectedItemProperty().addListener((ignored, oldValue, newValue) -> log.info("CHANGE: " + oldValue + " -> " + newValue));

		final var button = new Button("Print selection");
		button.setOnAction(ignored -> log.info("SELECTION: " + String.join(", ", listView.getSelectionModel().getSelectedItems())));

		final var scene = new Scene(new VBox(instructions, listView, button), 350, 400);

		final var width = scene.widthProperty().map(schemeWidth -> {
			var insets = instructions.getPadding();
			return schemeWidth.doubleValue() - insets.getLeft() - insets.getRight();
		});
		stream(paragraphs).forEach(paragraph -> paragraph.wrappingWidthProperty().bind(width));

		stage.setTitle("JavaFX ListView multiple unselect");
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}
}
